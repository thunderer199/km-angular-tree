var app = angular.module('testApp', ['kmTree']);

app.controller('testCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
    $scope.selectedNode = {id: "01.1.1.1", description: ""};
    $scope.selectedNode1 = {id: "01.1.2", description: ""};
    $scope.data = {
        50200000: {
            50201700: {
                10000111: {
                    description: "Coffee - Beans/Ground"
                },
                10000114: {
                    description: "Coffee - Ready to Drink"
                },
                10000115: {
                    description: "Coffee - Instant"
                },
                10000116: {
                    description: "Tea - Bags/Loose"
                },
                10000117: {
                    description: "Tea - Instant"
                },
                10000118: {
                    description: "Tea - Ready to Drink"
                },
                10000119: {
                    description: "Fruit Herbal Infusions - Bags/Loose"
                },
                10000210: {
                    description: "Fruit Herbal Infusions - Instant"
                },
                10000313: {
                    description: "Fruit Herbal Infusions - Ready to Drink"
                },
                10000592: {
                    description: "Coffee/Tea/Substitutes Variety Packs"
                },
                10006310: {
                    description: "Coffee Substitutes - Regular(Non-Instant)"
                },
                10006311: {
                    description: "Coffee Substitutes - Instant"
                },
                10006312: {
                    description: "Coffee Substitutes - Ready to Drink"
                },
                description: "Coffee/Tea/Substitutes"
            },
            description: "Beverages"
        }
    };

    $scope.fc = {};

    $timeout(function () {
        $scope.fc = {
            "01": {
                "description": "DAIRY AND SIMILAR PRODUCTS",
                "1": {
                    "description": "Milk and dairy-based drinks",
                    "1": {
                        "description": "Milk and buttermilk (plain)",
                        "1": {"description": "Milk (plain)"},
                        "2": {"description": "Buttermilk (plain)"}
                    },
                    "2": {"description": "Dairy-based drinks, flavoured and/or fermented (e.g., chocolate milk, cocoa, eggnog, drinking yoghurt, whey-based drinks)"}
                },
                "2": {
                    "description": "Fermented and renneted milk products (plain), excluding food category 1.2 (dairy-based drinks)",
                    "1": {
                        "description": "Fermented milks (plain)",
                        "1": {"description": "Fermented milks (plain), not heat-treated after fermentation"},
                        "2": {"description": "Fermented milks (plain), heat-treated after fermentation"}
                    },
                    "2": {"description": "Renneted milk (plain)"}
                },
                "3": {
                    "description": "Condensed milk and analogues (plain)",
                    "1": {"description": "Condensed milk (plain)"},
                    "2": {"description": "Beverage whiteners"}
                },
                "4": {
                    "description": "Cream (plain) and the like",
                    "1": {"description": "Pasteurized cream (plain)"},
                    "2": {"description": "Sterilized and UHT creams, whipping and whipped creams, and reduced fat creams (plain)"},
                    "3": {"description": "Clotted cream (plain)"},
                    "4": {"description": "Cream analogues"}
                },
                "5": {
                    "description": "Milk powder and cream powder and powder analogues (plain)",
                    "1": {"description": "Milk powder and cream powder (plain)"},
                    "2": {"description": "Milk and cream powder analogues"}
                },
                "6": {
                    "description": "Cheese and analogues",
                    "1": {"description": "Unripened cheese"},
                    "2": {
                        "description": "Ripened cheese",
                        "1": {"description": "Ripened cheese, includes rind"},
                        "2": {"description": "Rind of ripened cheese"},
                        "3": {"description": "Cheese powder (for reconstitution; e.g., for cheese sauces)"}
                    },
                    "3": {"description": "Whey cheese"},
                    "4": {
                        "description": "Processed cheese",
                        "1": {"description": "Plain processed cheese"},
                        "2": {"description": "Flavoured processed cheese, including containing fruit, vegetables, meat, etc."}
                    },
                    "5": {"description": "Cheese analogues"},
                    "6": {"description": "Whey protein cheese"}
                },
                "7": {"description": "Dairy-based desserts (e.g., pudding, fruit or flavoured yoghurt)"},
                "8": {
                    "description": "Whey and whey products, excluding whey cheeses",
                    "1": {"description": "Liquid whey and whey products, excluding whey cheeses"},
                    "2": {"description": "Dried whey and whey products, excluding whey cheeses"}
                }
            },
            "04": {
                "description": "FRUITS AND VEGETABLES, SEAWEEDS, AND NUTS AND SEEDS",
                "1": {
                    "description": "Fruit",
                    "1": {
                        "description": "Fresh fruit",
                        "1": {"description": "Untreated fresh fruit"},
                        "2": {"description": "Surface-treated fresh fruit"},
                        "3": {"description": "Peeled or cut fresh fruit"}
                    },
                    "2": {
                        "description": "Processed fruit",
                        "1": {"description": "Frozen fruit"},
                        "2": {"description": "Dried fruit"},
                        "3": {"description": "Fruit in vinegar, oil, or brine"},
                        "4": {"description": "Canned or bottled (pasteurized) fruit"},
                        "5": {"description": "Jams, jellies, marmelades"},
                        "6": {"description": "Fruit-based spreads (e.g., chutney) excluding products of food category 04.1.2.5"},
                        "7": {"description": "Candied fruit"},
                        "8": {"description": "Fruit preparations, including pulp, purees, fruit toppings and coconut milk"},
                        "9": {"description": "Fruit-based desserts, including fruit-flavoured water-based desserts"},
                        "10": {"description": "Fermented fruit products"},
                        "11": {"description": "Fruit fillings for pastries"},
                        "12": {"description": "Cooked fruit"}
                    }
                }
            }
        };
    }, 1000);

    $scope.readonlyValue = false;
}]);
