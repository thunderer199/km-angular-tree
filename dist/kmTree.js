var app = angular.module("kmTree", ['ntt.TreeDnD', "ui.bootstrap"]);

try {
    angular.module('schemaForm').config(['schemaFormDecoratorsProvider',
        function (schemaFormDecoratorsProvider) {
            schemaFormDecoratorsProvider.addMapping(
                'bootstrapDecorator',
                'treeKm',
                'pluginTree.html'
            );
        }
    ]);
} catch (e) {
}

app.controller('treeCtrl', ['$scope', '$http', 'dataService',
    function ($scope, $http, dataService) {


        // ms from 01-01-2015
        var time = new Date().getTime() - new Date('01-01-2015').getTime();

        // for multiple trees on page
        $scope.modalWindowName = 'treeView' + time + Math.random().toString(10).substring(2, 4);

        $scope.status = {
            isOpened: false
        };
        $scope.toggled = function(open) {
          if(open) {
              $scope.status.isOpened = true;
          }
        };

        $scope.my_tree = {};

        $scope.$watch('id_node.id', function (e) {
            if ($scope.id_node) {
                $scope.id_node.description = '';
                $scope.selectedNode = {id: null, description: null};
            }
            if (!e) return;

            var node = find($scope.tree_data, e);
            if (node !== null) {
                $scope.id_node = angular.copy(node);
                $scope.my_tree.select_node(node);
                setSelectedNodeData(node);
            }
        });

        function setSelectedNodeData(node) {
            var cloneNode = angular.copy(node);
            $scope.selectedNode.id = cloneNode.id;
            if($scope.currentLanguage) {
                if(!$scope.selectedNode.description) {
                    $scope.selectedNode.description = {};
                }
                $scope.selectedNode.description[$scope.currentLanguage] = cloneNode.description;
            } else {
                $scope.selectedNode.description = cloneNode.description;
            }
        }

        $scope.setSelectionOnTreeBySelectedNode = function (e) {
            if (e === null || e === undefined) return;

            if (e === '') {
                $scope.id_node = null;
                return;
            }


            var node = find($scope.tree_data, e);
            if ($scope.validation) $scope.validate(e, node);

            if (node !== null && node.__children__.length === 0) {
                $scope.id_node = angular.copy(node);
                if ($scope.my_tree.select_node) {
                    $scope.my_tree.select_node(node);
                }
                setSelectedNodeData(node);
            } else {
                $scope.id_node = null;
            }
        };
        $scope.$watch('selectedNode.id', $scope.setSelectionOnTreeBySelectedNode);

        function find_recursion(data, val) {
            if (!data) return null;

            if (data instanceof Array) {
                for (var i = 0; i < data.length; i++) {
                    var found = find_recursion(data[i], val);
                    if (found) return found;
                }
            } else {
                if (data.id == val) return data;
                else return find_recursion(data.__children__, val);
            }

            return null;
        }

        function findById(data, id) {
            if(data != null && data !== undefined) {
                if (!(data instanceof Array)) {
                    if (!data.hasOwnProperty('__children__')) return null;
                    data = data.__children__;
                }

                for (var i = 0; i < data.length; i++) {
                    if (data[i].id === id) return data[i];
                }
            }
            return null;
        }

        function find_hierarchy(data, val) {
            if (!data) return null;

            var path = val.split('.');
            var currNode = data;
            var idx = '';
            while (path.length > 0 && (currNode !== null || currNode !== undefined)) {
                idx = idx === '' ? path.shift() : idx + '.' + path.shift();
                currNode = findById(currNode, idx);
            }

            return currNode;
        }

        function find(data, val) {
            var intermNode = $scope.hierarchyKey ? find_hierarchy(data, val) : find_recursion(data, val);
            return intermNode;
        }

        // TODO: fix for bug in DND-TREE
        $scope.my_tree.on_select = function (node) {
            if (node.__children__.length === 0) {
                $scope.id_node = angular.copy(node);
                $scope.status.isOpened = false;
            } else {
                $scope.status.isOpened = true;
            }
            $scope.$apply();
        };

        $scope.my_tree.my_tree_handler = function (node) {
            $scope.my_tree.on_select(node);
        };

        $scope.expanding_property = {
            field: 'Name',
            titleClass: 'text-center',
            cellClass: 'v-middle',
            displayName: 'Name'
        };

        $scope.col_defs = [
            {
                field: 'id',
                displayName: 'Id'
            },
            {
                field: 'description',
                displayName: 'Description'
            }
        ];

        $scope.$watch('kmData', function (value) {
            $scope.tree_data = dataService.kmTree2dndTree(value, $scope.hierarchyKey);
            $scope.expandLevel = 1;
            if($scope.selectedNode) {
                $scope.setSelectionOnTreeBySelectedNode($scope.selectedNode.id);
            }
        });

        $scope.$watch('kmUrl', function (value) {
            if (!value) return;
            $scope.currentLanguage = angular.copy(value);
            if($scope.currentLanguage.slice(-1) === "/") {
                $scope.currentLanguage = $scope.currentLanguage.slice(0, -1);
            }
            $scope.currentLanguage = $scope.currentLanguage.split("/").slice(-1)[0];


            var tempData = angular.copy($scope.kmData); // backup
            $http.get(value).then(function (response) {
                $scope.kmData = response.data;
            }).catch(function () {
                $scope.kmData = tempData;
            });
        });

        $scope.validate = function (selectedNodeId, node) {
            if (!node) node = find($scope.tree_data, selectedNodeId);
            $scope.valid = node !== null;
            $scope.$emit('schemaForm.error.tree', 'tv4-500', $scope.valid);
        };
    }
]);


app.factory('dataService', function () {

    function recursion(data) {
        if (data === undefined || data === null) return;
        var keys = Object.keys(data);

        var idx = keys.indexOf('description');
        if (idx !== -1) {
            keys.splice(idx, 1);
        }

        return keys.map(function (e) {
            var obj = data[e];
            return {id: e, description: obj.description, __children__: recursion(obj)};
        });
    }

    function hierarchyRecursion(data, parentKey) {
        if (data === undefined || data === null) return;
        var keys = Object.keys(data);


        var idx = keys.indexOf('description');
        if (idx !== -1) {
            keys.splice(idx, 1);
        }

        return keys.map(function (e) {
            var obj = data[e];

            var currKey = angular.copy(parentKey);
            currKey.push(e);
            var id = currKey.join('.');
            return {id: id, description: obj.description, __children__: hierarchyRecursion(obj, currKey)};
        });

    }

    return {
        kmTree2dndTree: function (data, hierarchyKey) {
            return hierarchyKey ? hierarchyRecursion(data, []) : recursion(data);
        }
    };
});

app.directive('kmTreeView', function () {
    return {
        controller: 'treeCtrl',
        restrict: 'E',
        scope: {
            kmData: '=', selectedNode: '=',
            readonly: '=', form: '=', hierarchyKey: '@', validation: '@',
            expandLevel: '@', placeholder: '@', kmUrl: '@'
        },
        templateUrl: 'kmTree.html'
    };
});
angular.module("kmTree").run(["$templateCache", function($templateCache) {$templateCache.put("kmTree.html","<script>\n\n</script><ul class=\"nav pull-right\" style=\"width: 100%;\"><li class=\"btn-group dropdown\" style=\"width: 100%;\"><div><div style=\"display: block;\" class=\"input-group col-lg-6 col-md-6 col-sm-6 col-xs-6\"><input style=\"margin-top: 1px;\" class=form-control type=text ng-model=selectedNode.id ng-readonly=readonly schema-validate=form placeholder={{placeholder}}></div><div class=\"input-group col-lg-6 col-md-6 col-sm-6 col-xs-6\"><input style=\"z-index: 2; position: relative; width: 100%; white-space: nowrap; overflow: hidden; text-overflow: ellipsis\" class=form-control ng-model=id_node.description> <span class=input-group-btn uib-dropdown=\"\" on-toggle=toggled(open) auto-close=outsideClick is-open=status.isOpened><button class=\"btn btn-default dropdown-toggle\" uib-dropdown-toggle=\"\" id={{modalWindowName}} style=\"z-index: 2; position: relative;\" ng-if=!readonly><span class=\"glyphicon glyphicon-list-alt\" aria-hidden=true></span></button><div class=\"dropdown-menu dropdown-menu-right\" style=\"padding: 15px; padding-bottom: 0px;\" uib-dropdown-menu=\"\" aria-labelledby={{modalWindowName}}><div class=pre-scrollable><div style=\"min-width: 600px; width: 50%;word-wrap: break-word; white-space: normal; text-overflow: ellipsis;\"><tree-dnd expand-level=expandLevel tree-data=tree_data tree-control=my_tree primary-key=id column-defs=col_defs on-select=my_tree_handler(node) expand-on=expanding_property></tree-dnd></div></div></div></span></div></div></li></ul>");
$templateCache.put("pluginTree.html","<div class=form-group ng-class=\"{\'has-error\': hasError(), \'has-success\': form.disableSuccessState !== true && hasSuccess()}\"><label class=control-label ng-show=showTitle()>{{form.title}}</label><km-tree-view km-data=form.options.data hierarchy-key={{form.options.hierarchyKey}} km-url={{form.options.dataUrl}} expand-level=1 validation=true placeholder=\"\" selected-node=$$value$$ readonly=form.readonly form=form></km-tree-view><span class=help-block sf-message=form.description></span></div>");}]);