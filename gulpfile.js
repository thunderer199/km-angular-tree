var gulp = require('gulp');
var uglify = require('gulp-uglify');
var templatecache = require('gulp-angular-templatecache');
var minifyHtml = require('gulp-minify-html');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var addStream = require('add-stream');

function prepareTemplates() {
    return gulp.src('src/template/*.html')
        .pipe(minifyHtml({
            conditionals: true,
            spare: true
        }))
        .pipe(templatecache({
            module: 'kmTree'
        }));
}


gulp.task('minifyDist', function () {
    return gulp.src('./src/*.js')
        .pipe(rename('kmTree.min.js'))
        .pipe(addStream.obj(prepareTemplates()))
        .pipe(concat('kmTree.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/'));
});

gulp.task('dist', ['minifyDist'], function () {
    return gulp.src('./src/*.js')
        .pipe(addStream.obj(prepareTemplates()))
        .pipe(concat('kmTree.js'))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function () {
    gulp.watch(['./src/*.js', './src/template/*.html'], ['dist']);
});